#!/usr/bin/python3

from argparse import ArgumentParser
from json_handler import JsonHandler
import numpy as np

def makeParser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument("filename", type=str, help="json file where the camera matrices\
                                           are stored")
    return parser

if __name__ == "__main__":
    parser = makeParser()
    args = parser.parse_args()
    m = JsonHandler.read(args.filename)

    mat_x = np.asarray(m["matrix_x"])
    mat_y = np.asarray(m["matrix_y"])
    
    # do something with mat_x and mat_y